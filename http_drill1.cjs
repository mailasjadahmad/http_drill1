const http = require("http");
const fs = require("fs");
const path = require("path");
const url = require("url");
const uuid = require("uuid");

const server = http.createServer((req, res) => {
  let parse = url.parse(req.url, true);
  let pathname = parse.pathname;
  // console.log(pathname)
  if (req.method === "GET" && pathname === "/html") {
    console.log(req.method);
    const htmlFilePath = path.join(__dirname, "index.html");
    // console.log(__dirname)
    fs.readFile(htmlFilePath, "utf8", (err, data) => {
      if (err) {
        res.writeHead(500, { "Content-Type": "text/plain" });
        res.end("Internal Server Error");
        console.error(err);
      } else {
        res.writeHead(200, { "Content-Type": "text/html" });
        res.end(data);
      }
    });
  } else if (req.method === "GET" && pathname === "/json") {
    const jsonData = {
      slideshow: {
        author: "Yours Truly",
        date: "date of publication",
        slides: [
          {
            title: "Wake up to WonderWidgets!",
            type: "all",
          },
          {
            items: [
              "Why <em>WonderWidgets</em> are great",
              "Who <em>buys</em> WonderWidgets",
            ],
            title: "Overview",
            type: "all",
          },
        ],
        title: "Sample Slide Show",
      },
    };

    res.writeHead(200, { "Content-Type": "application/json" });
    res.end(JSON.stringify(jsonData));
  } 
  else if (req.method === "GET" && pathname === "/uuid") {
    const uuidName = uuid.v4();
    console.log(uuidName);

    res.writeHead(200, { "Content-Type": "text/html" });
    res.end(JSON.stringify(uuidName));
  } 
  else if (req.method === "GET" && pathname.includes("/status")) {
    // let query = pathname.split("/").pop();
    const decodedPath = decodeURIComponent(pathname);

    const urlArr = decodedPath.split("/");
    console.log(urlArr);
    res.writeHead(200, { "Content-Type": "text/html" });
    res.end(urlArr[urlArr.length - 1]);
  } 
  else if (req.method === "GET" && pathname.includes("/delay")) {
    const decodedPath = decodeURIComponent(pathname);
    console.log(decodedPath);
    const urlArr = decodedPath.split("/");
    const delayString = urlArr[urlArr.length - 1];
    let delayTime = delayString.match(/\d+/g).map(Number);
    // console.log(delaytime)
    delayTime = delayTime[0];

    setTimeout(() => {
      res.writeHead(200, { "Content-Type": "text/html" });
      res.end(`Status Code: ${res.statusCode}`);
    }, delayTime * 1000);
  } else {
    res.writeHead(404, { "Content-Type": "text/plain" });
    res.end("Not Found");
  }
});

server.listen(5000, () => {
  console.log("Server is listening on port 5000");
});
